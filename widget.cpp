﻿#include "widget.h"
#include "ui_widget.h"
#include <QStringLiteral>
#include <QMap>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    setWindowTitle("Qt-BDMap by yunke120");

    QString htmlPath = QCoreApplication::applicationDirPath() + "/html/";
    QString htmlFile = htmlPath + "index.html";
    qDebug() << htmlFile;
    QFile file(htmlFile);
    if(!file.exists())
        qDebug() << "html file is not exist";
    QWebChannel *webChannel = new QWebChannel(ui->widget->page());
    ui->widget->page()->setWebChannel(webChannel);
    webChannel->registerObject(QString("JSInterface"), ui->widget);

    ui->widget->page()->load(QUrl("file:///" + htmlFile));

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_2_clicked()
{
    QString str = ui->lineEdit->text();
    QString lon = str.split(",")[0];
    QString lat = str.split(",")[1];
    QString cmd=QString("drawMarker(%1,%2)").arg(lon).arg(lat);
    ui->widget->page()->runJavaScript(cmd);
}
